const { app, BrowserWindow, Menu, dialog, ipcMain } = require('electron')
const path = require('path')

/*

 __       __  ______  __    __  _______    ______   __       __   ______  
|  \  _  |  \|      \|  \  |  \|       \  /      \ |  \  _  |  \ /      \ 
| $$ / \ | $$ \$$$$$$| $$\ | $$| $$$$$$$\|  $$$$$$\| $$ / \ | $$|  $$$$$$\
| $$/  $\| $$  | $$  | $$$\| $$| $$  | $$| $$  | $$| $$/  $\| $$| $$___\$$
| $$  $$$\ $$  | $$  | $$$$\ $$| $$  | $$| $$  | $$| $$  $$$\ $$ \$$    \ 
| $$ $$\$$\$$  | $$  | $$\$$ $$| $$  | $$| $$  | $$| $$ $$\$$\$$ _\$$$$$$\
| $$$$  \$$$$ _| $$_ | $$ \$$$$| $$__/ $$| $$__/ $$| $$$$  \$$$$|  \__| $$
| $$$    \$$$|   $$ \| $$  \$$$| $$    $$ \$$    $$| $$$    \$$$ \$$    $$
 \$$      \$$ \$$$$$$ \$$   \$$ \$$$$$$$   \$$$$$$  \$$      \$$  \$$$$$$ 
                                                                          
                                                                          
                                                                          

*/

const createWindow = () => {
  const win = new BrowserWindow({
    width: 1920,
    height: 1080,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    }
  })
  
  win.loadFile('index.html')

}

const formWindow = () => {
  const win = new BrowserWindow({
    width: 600,
    height: 500,
    title: 'Mon formulaire',
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    }
  })
  
  win.loadFile('formulaire.html')

  win.on('close', e => {
    let choice = dialog.showMessageBox(
    win,
      {
        type: 'question',
        buttons: ['Yes', 'No'],
        title: 'Confirm',
        message: 'Do you really want to close the application?'
      }
    )
    if (choice === 1) e.preventDefault()
  })
}

/*


 ________  __    __  _______         __       __  ______  __    __  _______    ______   __       __   ______  
|        \|  \  |  \|       \       |  \  _  |  \|      \|  \  |  \|       \  /      \ |  \  _  |  \ /      \ 
| $$$$$$$$| $$\ | $$| $$$$$$$\      | $$ / \ | $$ \$$$$$$| $$\ | $$| $$$$$$$\|  $$$$$$\| $$ / \ | $$|  $$$$$$\
| $$__    | $$$\| $$| $$  | $$      | $$/  $\| $$  | $$  | $$$\| $$| $$  | $$| $$  | $$| $$/  $\| $$| $$___\$$
| $$  \   | $$$$\ $$| $$  | $$      | $$  $$$\ $$  | $$  | $$$$\ $$| $$  | $$| $$  | $$| $$  $$$\ $$ \$$    \ 
| $$$$$   | $$\$$ $$| $$  | $$      | $$ $$\$$\$$  | $$  | $$\$$ $$| $$  | $$| $$  | $$| $$ $$\$$\$$ _\$$$$$$\
| $$_____ | $$ \$$$$| $$__/ $$      | $$$$  \$$$$ _| $$_ | $$ \$$$$| $$__/ $$| $$__/ $$| $$$$  \$$$$|  \__| $$
| $$     \| $$  \$$$| $$    $$      | $$$    \$$$|   $$ \| $$  \$$$| $$    $$ \$$    $$| $$$    \$$$ \$$    $$
 \$$$$$$$$ \$$   \$$ \$$$$$$$        \$$      \$$ \$$$$$$ \$$   \$$ \$$$$$$$   \$$$$$$  \$$      \$$  \$$$$$$ 
                                                                                                              
                                                                                                              
                                                                                                              
*/



const mainMenuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Exit',
        click() {
          app.quit()
        }
      },      
    ]
  },
  {
    label: 'Form',
    click() {
      formWindow()
    }
  }
]

app.whenReady().then(() => {
    ipcMain.on('set-name', handleSetName)
    createWindow()

    app.on('activate', () => {
      if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })

    mainMenuTemplate.push({
      label: 'Developer Tools',
      submenu: [
        {
          label: 'Toggle DevTools',
          accelerator: process.platform ==='darwin' ? 'Command+I' :'Ctrl+I',
          click(item, focusedWindow) {
            focusedWindow.toggleDevTools()
        }
      },
        {
          role: 'reload'
        }
      ]
    })
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)
    Menu.setApplicationMenu(mainMenu)

    // console.log(process.env.NODE_ENV);
    // if (process.env.NODE_ENV !== 'production') {

    // }      

 })

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})


function handleSetName (event, data) {
  // console.log(data);
  const webContents = event.sender
  const win = BrowserWindow.fromWebContents(webContents)
  // win.setName(data)
  win.webContents.send('set-name', data);
}